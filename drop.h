void copy_to_tmp(const char* name){
	char command[1000];	
	command[0]='\0';
	strcat(command,"./scripts/copy_files.sh ");
	strcat(command,name);
	system(command);	
}

void remove_table_main(const char* name){
	char command[1000];
	command[0]='\0';
	strcat(command,"./scripts/remove_tab_main.sh ");
	strcat(command,name);
	strcat(command," 2");
	system(command);	
}

void remove_table(const char* name){
	char command[1000];
	command[0]='\0';
	strcat(command,"./scripts/remove_files.sh ");
	strcat(command,name);
	strcat(command," 1");
	system(command);
	remove_table_main(name);	
}


int check_exist_tmp(const char* name){
	int i;	
	char command[1000];
	command[0]='\0';
	strcat(command,"./scripts/check_exist_tmp.sh ");
	strcat(command,name);
	strcat(command,".dat");
	i=system(command);
	return i;
}
