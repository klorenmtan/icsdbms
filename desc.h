using namespace std;
std::vector<std::string> show_table_contents(const char* table_name){
	char filename[1000];	
	int c=0;	
	filename[0]='\0';
	strcat(filename,"data/tmp/");	
	strcat(filename,table_name);
	strcat(filename,".stdat");
	//puts(filename);

	std::ifstream infile(filename, ios::in | ios::binary);    // we don't really care where you got the name from
	std::vector<std::string> values;

	for (std::string line; std::getline(infile, line, ' '); ){
    line.erase(std::remove(line.begin(), line.end(), ' '), line.end());
    values.push_back(std::move(line));
	}
	return values;
}

void print_table_properties(vector<string> &values){
	int counter=1;	
	printf("column name \t data type \n");
	printf("========================================================\n");
	int no_of_tokens = atoi(values[0].c_str());
	for(int i=1;i<values.size();i++){	
	
	if((strcmp(values[i].c_str(),"int")==0) || (strcmp(values[i].c_str(),"float")==0) || (strcmp(values[i].c_str(),"char")==0) || (strcmp(values[i].c_str(),"boolean")==0) || (strcmp(values[i].c_str(),"date")==0)){
		//printf(" %s",values[i].c_str());
		cout << values[i] << " ";
		}
		//constraints
		else if((strcmp(values[i].c_str(),"primary_key")==0) || (strcmp(values[i].c_str(),"unique")==0) || (strcmp(values[i].c_str(),"not_null")==0)){
		cout << values[i] << " ";
		}
		//varchar special case
		else if(strcmp(values[i].c_str(),"varchar")==0){
			cout << values[i] << " ";
			cout << values[i+1] << " ";
			i++;
			
		}
		//for column names 
		else{
			if(counter==1){	cout << values[i] << "\t\t"; counter++;}
			else{ cout << endl; cout << values[i] << "\t\t"; }	
		}


	}//end of for
	cout << endl;
}
