using namespace std;
std::vector<string> listFiles( const char* path, vector<string> files ){
	DIR* dirFile = opendir( path );
	char *str3;
	str3[0] = '\0';
	std::string str;
	std::vector <string> names;

   const char* str2 = str.c_str();
	if ( dirFile ){
      struct dirent* hFile;
      while (( hFile = readdir( dirFile )) != NULL ){
		
         if ( !strcmp( hFile->d_name, "."  )) continue;
         if ( !strcmp( hFile->d_name, ".." )) continue;
		 if ( strstr( hFile->d_name, ".dat" )){
			str3= (char*) malloc ((strlen(hFile->d_name)-3));
			memmove(str3, hFile->d_name, strlen(hFile->d_name)-4);
			//cout << str3 << endl;
			files.push_back(str3);
            }
      } 
      closedir( dirFile );
   }
	
	return files;
}

void print_table_names(vector<string> &files, vector<string> &status){
	int no_contents = status.size();
	int j=0;
	
	
	std::sort(files.begin(),files.end());
	
	cout << "Table Names" <<endl;	
	for (int i = 0;i < files.size();i++) {
		if(no_contents==0) cout << files[i] << endl;
		else{
			for(j=0;j<status.size();j++){
			if((strcmp(files[i].c_str(),status[j].c_str()))!=0){
				cout << files[i] << endl;				
				}
			}	
		}
	}		
		cout << "========================================" << endl;
		cout << (files.size()-no_contents) << " Tables" << endl;
	
}//end of print_table_names


